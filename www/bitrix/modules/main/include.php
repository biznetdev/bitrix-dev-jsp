<?php
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2013 Bitrix
 */

use Bitrix\Main\Session\Legacy\HealerEarlySessionStart;

require_once(__DIR__."/bx_root.php");
require_once(__DIR__."/start.php");

$application = \Bitrix\Main\Application::getInstance();
$application->initializeExtendedKernel(array(
	"get" => $_GET,
	"post" => $_POST,
	"files" => $_FILES,
	"cookie" => $_COOKIE,
	"server" => $_SERVER,
	"env" => $_ENV
));

//define global application object
$GLOBALS["APPLICATION"] = new CMain;

if(defined("SITE_ID"))
	define("LANG", SITE_ID);

if(defined("LANG"))
{
	if(defined("ADMIN_SECTION") && ADMIN_SECTION===true)
		$db_lang = CLangAdmin::GetByID(LANG);
	else
		$db_lang = CLang::GetByID(LANG);

	$arLang = $db_lang->Fetch();

	if(!$arLang)
	{
		throw new \Bitrix\Main\SystemException("Incorrect site: ".LANG.".");
	}
}
else
{
	$arLang = $GLOBALS["APPLICATION"]->GetLang();
	define("LANG", $arLang["LID"]);
}

if($arLang["CULTURE_ID"] == '')
{
	throw new \Bitrix\Main\SystemException("Culture not found, or there are no active sites or languages.");
}

$lang = $arLang["LID"];
if (!defined("SITE_ID"))
	define("SITE_ID", $arLang["LID"]);
define("SITE_DIR", ($arLang["DIR"] ?? ''));
define("SITE_SERVER_NAME", ($arLang["SERVER_NAME"] ?? ''));
define("SITE_CHARSET", $arLang["CHARSET"]);
define("FORMAT_DATE", $arLang["FORMAT_DATE"]);
define("FORMAT_DATETIME", $arLang["FORMAT_DATETIME"]);
define("LANG_DIR", ($arLang["DIR"] ?? ''));
define("LANG_CHARSET", $arLang["CHARSET"]);
define("LANG_ADMIN_LID", $arLang["LANGUAGE_ID"]);
define("LANGUAGE_ID", $arLang["LANGUAGE_ID"]);

$culture = \Bitrix\Main\Localization\CultureTable::getByPrimary($arLang["CULTURE_ID"], ["cache" => ["ttl" => CACHED_b_lang]])->fetchObject();

$context = $application->getContext();
$context->setLanguage(LANGUAGE_ID);
$context->setCulture($culture);

$request = $context->getRequest();
if (!$request->isAdminSection())
{
	$context->setSite(SITE_ID);
}

$application->start();

$GLOBALS["APPLICATION"]->reinitPath();

if (!defined("POST_FORM_ACTION_URI"))
{
	define("POST_FORM_ACTION_URI", htmlspecialcharsbx(GetRequestUri()));
}

$GLOBALS["MESS"] = [];
$GLOBALS["ALL_LANG_FILES"] = [];
IncludeModuleLangFile(__DIR__."/tools.php");
IncludeModuleLangFile(__FILE__);

error_reporting(COption::GetOptionInt("main", "error_reporting", E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR|E_PARSE) & ~E_STRICT & ~E_DEPRECATED);

if(!defined("BX_COMP_MANAGED_CACHE") && COption::GetOptionString("main", "component_managed_cache_on", "Y") <> "N")
{
	define("BX_COMP_MANAGED_CACHE", true);
}

// global functions
require_once(__DIR__."/filter_tools.php");

define('BX_AJAX_PARAM_ID', 'bxajaxid');

/*ZDUyZmZM2UxZDdkNmM2NTZmZTU1MDM4MWI0MWZiMjExMzdlNWI=*/$GLOBALS['_____114420438']= array(base64_decode('R2V0TW9'.'k'.'dWxlRX'.'ZlbnRz'),base64_decode('RXhlY'.'3'.'V'.'0ZU'.'1'.'vZHVsZUV2ZW'.'5'.'0RXg='));$GLOBALS['____908293621']= array(base64_decode('ZGVmaW5l'),base64_decode('c3RybG'.'Vu'),base64_decode('Ym'.'FzZTY0X2RlY2'.'9kZ'.'Q'.'=='),base64_decode('dW5z'.'ZXJpY'.'Wxpem'.'U='),base64_decode('aXNfYXJy'.'YXk'.'='),base64_decode('Y2'.'9'.'1bn'.'Q='),base64_decode('aW5'.'fYXJ'.'yYXk='),base64_decode('c2VyaW'.'F'.'saXpl'),base64_decode('Ym'.'FzZTY0X2VuY29kZQ='.'='),base64_decode(''.'c3RybG'.'Vu'),base64_decode('YXJ'.'yY'.'Xlf'.'a2V'.'5'.'X'.'2'.'V'.'4aXN0cw=='),base64_decode('Y'.'X'.'JyY'.'Xlfa2'.'V5X2V4'.'aX'.'N0'.'cw=='),base64_decode('bWt'.'0'.'a'.'W1'.'l'),base64_decode('ZG'.'F0ZQ=='),base64_decode(''.'ZGF0'.'ZQ'.'=='),base64_decode(''.'YXJ'.'yYX'.'lfa2V5X2V4aXN0cw='.'='),base64_decode('c3R'.'ybGVu'),base64_decode('Y'.'XJyYXlfa'.'2V5X2V4aXN0'.'cw=='),base64_decode(''.'c'.'3R'.'ybGVu'),base64_decode(''.'YXJyYXl'.'f'.'a'.'2V5X2V4'.'aXN0cw'.'=='),base64_decode('YXJyY'.'Xlfa'.'2V5X2V4'.'aXN'.'0cw=='),base64_decode(''.'b'.'Wt0aW'.'1l'),base64_decode('ZG'.'F0'.'ZQ=='),base64_decode('Z'.'GF0Z'.'Q'.'=='),base64_decode('bW'.'V0aG'.'9k'.'X2V4aXN'.'0cw=='),base64_decode('Y2FsbF'.'91c2Vy'.'X2Z1b'.'mN'.'f'.'YXJyYXk='),base64_decode(''.'c3RybGVu'),base64_decode(''.'YXJ'.'yYXlfa2V5'.'X2V'.'4aX'.'N0cw=='),base64_decode('Y'.'XJyYXl'.'f'.'a2'.'V'.'5X2V'.'4aXN'.'0c'.'w=='),base64_decode('c2V'.'ya'.'WFs'.'aXpl'),base64_decode('YmFzZT'.'Y0X2V'.'uY29kZ'.'Q='.'='),base64_decode('c3'.'R'.'ybG'.'Vu'),base64_decode('YX'.'J'.'yY'.'Xlf'.'a2V5X2V4aXN0cw='.'='),base64_decode('YXJyYX'.'lf'.'a'.'2V5X2V4aXN0cw='.'='),base64_decode('YXJyYXlfa2V5'.'X2V4a'.'XN0c'.'w'.'=='),base64_decode('aXNfY'.'X'.'JyYXk='),base64_decode('YXJyYXl'.'fa2V5'.'X'.'2V4aXN'.'0'.'c'.'w=='),base64_decode(''.'c2'.'VyaWFs'.'aXpl'),base64_decode(''.'YmFzZ'.'TY0X2VuY29kZQ=='),base64_decode('YXJyY'.'Xlfa2V'.'5X'.'2V4aXN0cw'.'=='),base64_decode('YXJyYXlfa2'.'V5'.'X2'.'V4'.'aXN0'.'c'.'w=='),base64_decode('c2VyaWF'.'saXpl'),base64_decode(''.'Y'.'mFzZT'.'Y0X2V'.'uY29kZ'.'Q='.'='),base64_decode('aX'.'Nf'.'YX'.'Jy'.'YXk='),base64_decode('aXNfYXJyYX'.'k='),base64_decode('aW5fY'.'X'.'JyYXk='),base64_decode('YXJyYXlfa2V5'.'X2V4aX'.'N0c'.'w=='),base64_decode(''.'aW5fYXJ'.'yYXk'.'='),base64_decode(''.'bWt'.'0aW1l'),base64_decode('Z'.'GF0ZQ='.'='),base64_decode('ZGF0ZQ='.'='),base64_decode('ZGF0ZQ='.'='),base64_decode('b'.'Wt0'.'aW1l'),base64_decode('ZG'.'F0ZQ=='),base64_decode('ZGF0ZQ=='),base64_decode(''.'aW'.'5fYX'.'JyYXk='),base64_decode('YXJ'.'yYXlfa2V5X2V4aX'.'N0'.'cw'.'=='),base64_decode('YXJyYXl'.'f'.'a'.'2V5'.'X'.'2V4aXN0cw=='),base64_decode('c2Vya'.'WFsaXp'.'l'),base64_decode('YmF'.'zZ'.'TY0X2'.'V'.'u'.'Y'.'2'.'9kZQ=='),base64_decode('YXJyY'.'Xlfa'.'2'.'V5X2V4aXN0'.'c'.'w'.'='.'='),base64_decode('aW'.'50d'.'mFs'),base64_decode('dGl'.'tZ'.'Q=='),base64_decode(''.'YXJ'.'y'.'YXlfa2V5'.'X'.'2V'.'4aXN'.'0cw'.'=='),base64_decode('ZmlsZ'.'V9leGl'.'zdH'.'M'.'='),base64_decode('c3RyX3JlcG'.'xh'.'Y2'.'U='),base64_decode(''.'Y2xh'.'c3'.'NfZ'.'Xhpc'.'3Rz'),base64_decode('ZGVmaW5l'));if(!function_exists(__NAMESPACE__.'\\___1205907454')){function ___1205907454($_1172280659){static $_695050718= false; if($_695050718 == false) $_695050718=array('S'.'U5UU'.'kFORVRfR'.'URJVEl'.'PT'.'g==','WQ==',''.'bWFpb'.'g==','f'.'mN'.'wZ'.'l9tYX'.'BfdmFsdWU=','','ZQ==',''.'Z'.'g==',''.'ZQ'.'==','Rg==','W'.'A'.'==','Zg==',''.'bWFpbg='.'=','fmNwZl9tY'.'XBfd'.'m'.'FsdWU=',''.'U'.'G9yd'.'GFs','Rg'.'==','ZQ==','ZQ='.'=','WA==','Rg==','RA='.'=','R'.'A==','bQ'.'==','ZA'.'='.'=',''.'WQ==','Zg==','Zg==','Zg'.'==','Zg==',''.'U'.'G9y'.'dGFs','Rg==',''.'ZQ'.'='.'=','Z'.'Q='.'=','WA'.'==',''.'Rg==','RA==','RA'.'==','bQ==','ZA==','WQ==','b'.'WFpbg='.'=','T24=','U2V0dGluZ3N'.'Da'.'GFuZ2U=','Zg==','Zg'.'==','Zg==','Zg==','bW'.'Fpbg='.'=','fmNwZl9'.'tYXBfdm'.'FsdW'.'U'.'=','ZQ'.'==','ZQ==','ZQ==',''.'RA==','ZQ'.'==','ZQ==','Zg==',''.'Zg==','Zg==','ZQ==','bWFpbg='.'=','f'.'mN'.'w'.'Zl9t'.'Y'.'XBfd'.'m'.'FsdWU=','ZQ==','Zg='.'=','Zg==','Zg'.'==','Z'.'g'.'='.'=',''.'bWFpbg==','fmNwZl9tYXBf'.'dmFsdWU=','ZQ==',''.'Z'.'g'.'==','UG9ydGFs','U'.'G9y'.'d'.'GFs','ZQ==','ZQ==','UG9ydGF'.'s',''.'Rg==','WA'.'==','R'.'g'.'='.'=','R'.'A==',''.'ZQ==','ZQ==','R'.'A==','bQ'.'==','ZA==','WQ==','ZQ==','W'.'A==',''.'ZQ='.'=',''.'Rg==',''.'Z'.'Q==','RA'.'==','Zg'.'==','ZQ='.'=','RA==',''.'ZQ==','bQ'.'='.'=','ZA'.'==','WQ==','Zg'.'==','Z'.'g'.'==','Zg'.'==','Zg='.'=','Zg==',''.'Z'.'g='.'=','Zg==','Zg='.'=',''.'bWFpbg='.'=','fm'.'NwZl9t'.'YX'.'B'.'fdmFsd'.'WU=','ZQ==','ZQ==','UG9y'.'dGF'.'s','R'.'g==','W'.'A'.'='.'=','V'.'FlQR'.'Q'.'='.'=','R'.'EFURQ==','RkVBVFVS'.'R'.'V'.'M=','RVh'.'QSVJFRA==',''.'VFlQR'.'Q'.'==',''.'R'.'A==','VFJZX0RBWV'.'Nf'.'Q'.'09VT'.'lQ'.'=','REF'.'U'.'RQ==',''.'VFJZX0RBW'.'V'.'NfQ0'.'9VT'.'l'.'Q=','RV'.'hQSVJ'.'FRA='.'=','R'.'kVBVF'.'VSRVM=','Zg='.'=','Zg==','RE9DV'.'U'.'1FTlRf'.'Uk'.'9PVA==','L2Jp'.'dHJp'.'eC9t'.'b2R1bGVzL'.'w==','L2'.'luc'.'3'.'RhbGwvaW5'.'kZXg'.'u'.'cGhw','L'.'g'.'==','Xw==','c2Vh'.'cmNo','Tg==','','',''.'Q'.'UNUSVZF','WQ==','c2'.'9jaWFsbmV0'.'d'.'2'.'9yaw='.'=','YW'.'x'.'sb3dfZ'.'nJ'.'pZWxkcw==','W'.'Q==','SUQ=','c29j'.'a'.'WF'.'sbmV0d29yaw==','YWxsb3dfZnJp'.'ZW'.'xkcw='.'=','SUQ=',''.'c29jaW'.'FsbmV0d'.'2'.'9y'.'a'.'w'.'==',''.'YW'.'xsb3'.'d'.'fZ'.'nJp'.'ZWxkcw'.'==','Tg==','','','QU'.'NU'.'S'.'VZF','WQ==',''.'c29j'.'a'.'W'.'FsbmV0d'.'29'.'yaw==','YWxsb3df'.'bW'.'l'.'jcm9ibG'.'9nX3'.'VzZXI=','W'.'Q==','SUQ=','c29jaW'.'Fs'.'b'.'mV0d2'.'9'.'yaw='.'=',''.'YWxsb'.'3df'.'bWljcm9ibG9nX3VzZXI'.'=','SUQ'.'=',''.'c29jaWFsbmV0d'.'29yaw==','YWxsb3'.'d'.'fbWl'.'j'.'cm9i'.'bG9'.'nX3VzZXI=','c29j'.'aWFsb'.'m'.'V'.'0'.'d2'.'9y'.'aw==','YWxsb3dfbW'.'ljc'.'m9ibG9'.'n'.'X2dyb3Vw',''.'WQ==','SUQ=','c29jaWF'.'s'.'bmV'.'0d29yaw==','YWx'.'sb3dfb'.'Wljcm9ibG'.'9nX'.'2dyb3Vw','SUQ=',''.'c29jaWFsbmV0'.'d2'.'9'.'yaw==',''.'YWxsb3df'.'bW'.'lj'.'cm9'.'ibG9nX2'.'dy'.'b3'.'V'.'w',''.'T'.'g='.'=','','','QUNU'.'SVZF','WQ==',''.'c29jaWFsbm'.'V0'.'d29yaw==',''.'YWxsb'.'3dfZml'.'sZX'.'NfdXNlcg==','WQ==','SU'.'Q=','c'.'2'.'9j'.'aWFsbmV0d29y'.'aw==','YWxsb3'.'d'.'fZ'.'ml'.'sZXN'.'fdXN'.'lcg='.'=','S'.'UQ=','c29j'.'aWFsb'.'mV0d2'.'9ya'.'w==','YW'.'xsb3df'.'ZmlsZ'.'XNfdXNlcg==',''.'Tg==','','','QUNUSVZ'.'F','WQ==','c29ja'.'WFsbmV0d29yaw==',''.'YWxsb'.'3dfYm'.'x'.'vZ191c2Vy','WQ==','SUQ=',''.'c29ja'.'WFsbm'.'V0'.'d29yaw==','Y'.'Wxsb3dfY'.'m'.'xvZ'.'191'.'c2Vy','SUQ=','c29ja'.'WFsbmV0d2'.'9ya'.'w='.'=','YW'.'xsb3dfY'.'mxvZ191c2Vy','Tg==','','',''.'QUNUSV'.'ZF','W'.'Q==','c29jaWFsbmV0d29'.'yaw==',''.'YW'.'xsb'.'3'.'dfcGhv'.'dG'.'9'.'fdXNlcg='.'=',''.'WQ==','SUQ=','c'.'2'.'9j'.'aWFsbm'.'V0d29y'.'aw='.'=','YW'.'xs'.'b3'.'d'.'fcGhvdG9fdXNlcg==','SU'.'Q=','c'.'29jaWFsbmV'.'0d'.'29y'.'aw==','YWxsb3dfcGh'.'v'.'dG9fdXNlcg='.'=',''.'Tg==','','','QUNUSVZF','WQ'.'==',''.'c29jaWFsbm'.'V0d2'.'9y'.'aw==','YW'.'xs'.'b3'.'df'.'Zm9'.'ydW1fdXNlc'.'g==','WQ='.'=','S'.'UQ'.'=','c'.'29jaWFsbmV0d29'.'ya'.'w==','YWxs'.'b3'.'df'.'Zm9ydW1fdX'.'Nlcg==',''.'SU'.'Q=','c29'.'j'.'aWFsbmV0d29yaw==','YWx'.'sb'.'3'.'dfZm9y'.'dW1'.'fdX'.'Nl'.'cg'.'='.'=',''.'Tg==','','','QUNUSVZF','WQ==','c'.'29jaW'.'Fsb'.'mV'.'0d'.'2'.'9yaw==','YW'.'xs'.'b3'.'d'.'fd'.'G'.'Fza3'.'NfdX'.'N'.'lc'.'g==','WQ==','SUQ=','c29jaWF'.'s'.'bmV0d29yaw'.'==',''.'YW'.'xsb3dfdGFz'.'a'.'3N'.'fdXN'.'lcg==',''.'SUQ=','c29jaWFsbmV0d29yaw='.'=','YWxsb'.'3'.'dfdGFz'.'a3'.'NfdXNlcg==','c'.'2'.'9j'.'a'.'WFsbmV0d'.'29'.'y'.'a'.'w'.'='.'=','YWxs'.'b3dfdG'.'Fza3NfZ3J'.'vdXA=','WQ='.'=','S'.'UQ=','c29jaW'.'FsbmV0d'.'29y'.'aw='.'=','Y'.'Wxsb3d'.'fdGF'.'za3'.'NfZ3JvdX'.'A=','S'.'U'.'Q=','c29jaW'.'FsbmV0d29ya'.'w==','Y'.'Wxsb3dfdGFza3N'.'fZ3JvdXA'.'=','dGFza3M'.'=','Tg'.'==','','','QUNU'.'SVZ'.'F','WQ==',''.'c29ja'.'W'.'FsbmV0d29yaw==',''.'Y'.'Wxsb3'.'dfY2FsZW5'.'k'.'YXJfdXNlcg==','WQ==','S'.'UQ=','c29'.'jaWFs'.'bmV0d29y'.'aw==','YW'.'xsb3'.'dfY2FsZW'.'5k'.'YXJfdX'.'N'.'lcg='.'=',''.'S'.'UQ'.'=','c29jaWFsbmV0'.'d29yaw==','YWxsb3dfY2FsZW'.'5kYX'.'J'.'fdXNlc'.'g==','c29ja'.'WFsbmV0'.'d29yaw==','Y'.'Wxsb3d'.'fY2FsZW5'.'kYXJfZ3JvdXA=','WQ='.'=',''.'S'.'UQ=',''.'c29jaWFsbm'.'V0d29yaw==','Y'.'W'.'xsb3'.'dfY2FsZW5'.'k'.'YX'.'JfZ3Jv'.'dXA=','SUQ=','c29j'.'a'.'W'.'FsbmV0d29yaw'.'='.'=','YWxsb'.'3dfY2FsZW5kY'.'X'.'J'.'fZ'.'3'.'J'.'vdXA=','QU'.'NU'.'SV'.'ZF','WQ==',''.'Tg==','Z'.'Xh'.'0c'.'mFuZXQ'.'=',''.'aWJsb2Nr','T'.'25'.'BZnRlcklCbG'.'9ja0VsZW'.'1lb'.'nRVcGRh'.'dGU=','aW50'.'cmF'.'uZ'.'XQ'.'=','Q0lu'.'dHJhb'.'mV0RXZlbnRIYW5kbGVy'.'cw='.'=','U'.'1BS'.'ZW'.'dpc3RlclV'.'wZ'.'GF0ZWRJdG'.'Vt',''.'Q'.'0ludHJhbmV0'.'U2hh'.'cmVwb2lu'.'dDo6'.'QWdlbn'.'RMaXN0'.'cygpOw==','aW50cmFuZXQ=','T'.'g==','Q0'.'ludHJhbm'.'V0'.'U2'.'hhcm'.'Vwb2ludDo6QWdlbnR'.'Rd'.'WV1ZS'.'g'.'p'.'O'.'w'.'==','aW'.'50c'.'mFuZXQ'.'=','Tg==',''.'Q0lu'.'dHJhbmV0U2h'.'hc'.'mV'.'w'.'b2ludDo6'.'QWdlb'.'nRVc'.'GRh'.'dGUoKTs=','a'.'W5'.'0cmFuZXQ=','Tg'.'==','aWJsb'.'2Nr','T25B'.'ZnRlcklCbG9ja0'.'VsZW1lbnR'.'BZG'.'Q=','aW5'.'0cm'.'FuZXQ=',''.'Q0ludHJhb'.'mV0RXZlbnRIYW5k'.'bGVy'.'c'.'w='.'=','U1BSZ'.'Wdp'.'c'.'3RlclVwZGF0ZW'.'RJdG'.'Vt','aWJsb'.'2N'.'r',''.'T2'.'5BZnRlcklCbG9'.'ja0VsZ'.'W1lbnRVcGRhd'.'G'.'U=','aW50c'.'mFuZXQ'.'=','Q0l'.'ud'.'HJhb'.'mV0'.'RX'.'ZlbnRIYW5kbGVy'.'cw==','U1B'.'SZWdp'.'c3Rlcl'.'VwZGF0ZW'.'RJ'.'dGVt','Q0'.'ludHJhb'.'mV'.'0U2h'.'hc'.'mV'.'w'.'b2'.'ludDo6QWdlb'.'n'.'RMaXN0c'.'ygpOw==','aW50c'.'m'.'FuZXQ=','Q0ludHJhbmV0'.'U2h'.'hcmVwb2ludD'.'o6QWdlbnRRdWV'.'1ZS'.'gpOw==','a'.'W50'.'c'.'m'.'FuZX'.'Q=','Q0ludHJ'.'hb'.'mV0U'.'2hh'.'cmVw'.'b2l'.'u'.'dDo6QW'.'dlbnRVc'.'GR'.'h'.'dGUoK'.'Ts=','aW50cmFuZ'.'XQ=',''.'Y3Jt','bWFpbg==','T25C'.'ZW'.'Z'.'v'.'cmV'.'Qcm'.'9s'.'b2c=','bWFpb'.'g'.'==','Q1dpemFyZFNvbFB'.'hbmVsSW50cmFuZXQ=','U2hvd'.'1BhbmVs','L21v'.'ZHVsZX'.'Mva'.'W5'.'0cmFu'.'ZXQvcGF'.'uZWxfYnV'.'0'.'d'.'G9uLnBocA==','RU5'.'D'.'T0RF',''.'WQ'.'==');return base64_decode($_695050718[$_1172280659]);}};$GLOBALS['____908293621'][0](___1205907454(0), ___1205907454(1));class CBXFeatures{ private static $_148789789= 30; private static $_1006728797= array( "Portal" => array( "CompanyCalendar", "CompanyPhoto", "CompanyVideo", "CompanyCareer", "StaffChanges", "StaffAbsence", "CommonDocuments", "MeetingRoomBookingSystem", "Wiki", "Learning", "Vote", "WebLink", "Subscribe", "Friends", "PersonalFiles", "PersonalBlog", "PersonalPhoto", "PersonalForum", "Blog", "Forum", "Gallery", "Board", "MicroBlog", "WebMessenger",), "Communications" => array( "Tasks", "Calendar", "Workgroups", "Jabber", "VideoConference", "Extranet", "SMTP", "Requests", "DAV", "intranet_sharepoint", "timeman", "Idea", "Meeting", "EventList", "Salary", "XDImport",), "Enterprise" => array( "BizProc", "Lists", "Support", "Analytics", "crm", "Controller",), "Holding" => array( "Cluster", "MultiSites",),); private static $_784330868= false; private static $_1648334508= false; private static function __557755087(){ if(self::$_784330868 == false){ self::$_784330868= array(); foreach(self::$_1006728797 as $_826357854 => $_553949130){ foreach($_553949130 as $_1523780043) self::$_784330868[$_1523780043]= $_826357854;}} if(self::$_1648334508 == false){ self::$_1648334508= array(); $_286069999= COption::GetOptionString(___1205907454(2), ___1205907454(3), ___1205907454(4)); if($GLOBALS['____908293621'][1]($_286069999)>(196*2-392)){ $_286069999= $GLOBALS['____908293621'][2]($_286069999); self::$_1648334508= $GLOBALS['____908293621'][3]($_286069999); if(!$GLOBALS['____908293621'][4](self::$_1648334508)) self::$_1648334508= array();} if($GLOBALS['____908293621'][5](self::$_1648334508) <=(1260/2-630)) self::$_1648334508= array(___1205907454(5) => array(), ___1205907454(6) => array());}} public static function InitiateEditionsSettings($_1961714582){ self::__557755087(); $_134907010= array(); foreach(self::$_1006728797 as $_826357854 => $_553949130){ $_182914697= $GLOBALS['____908293621'][6]($_826357854, $_1961714582); self::$_1648334508[___1205907454(7)][$_826357854]=($_182914697? array(___1205907454(8)): array(___1205907454(9))); foreach($_553949130 as $_1523780043){ self::$_1648334508[___1205907454(10)][$_1523780043]= $_182914697; if(!$_182914697) $_134907010[]= array($_1523780043, false);}} $_1624041312= $GLOBALS['____908293621'][7](self::$_1648334508); $_1624041312= $GLOBALS['____908293621'][8]($_1624041312); COption::SetOptionString(___1205907454(11), ___1205907454(12), $_1624041312); foreach($_134907010 as $_777065327) self::__413272710($_777065327[min(172,0,57.333333333333)], $_777065327[round(0+0.33333333333333+0.33333333333333+0.33333333333333)]);} public static function IsFeatureEnabled($_1523780043){ if($GLOBALS['____908293621'][9]($_1523780043) <= 0) return true; self::__557755087(); if(!$GLOBALS['____908293621'][10]($_1523780043, self::$_784330868)) return true; if(self::$_784330868[$_1523780043] == ___1205907454(13)) $_1006949086= array(___1205907454(14)); elseif($GLOBALS['____908293621'][11](self::$_784330868[$_1523780043], self::$_1648334508[___1205907454(15)])) $_1006949086= self::$_1648334508[___1205907454(16)][self::$_784330868[$_1523780043]]; else $_1006949086= array(___1205907454(17)); if($_1006949086[(1240/2-620)] != ___1205907454(18) && $_1006949086[(164*2-328)] != ___1205907454(19)){ return false;} elseif($_1006949086[(170*2-340)] == ___1205907454(20)){ if($_1006949086[round(0+0.25+0.25+0.25+0.25)]< $GLOBALS['____908293621'][12]((878-2*439),(994-2*497),(230*2-460), Date(___1205907454(21)), $GLOBALS['____908293621'][13](___1205907454(22))- self::$_148789789, $GLOBALS['____908293621'][14](___1205907454(23)))){ if(!isset($_1006949086[round(0+2)]) ||!$_1006949086[round(0+0.5+0.5+0.5+0.5)]) self::__60063368(self::$_784330868[$_1523780043]); return false;}} return!$GLOBALS['____908293621'][15]($_1523780043, self::$_1648334508[___1205907454(24)]) || self::$_1648334508[___1205907454(25)][$_1523780043];} public static function IsFeatureInstalled($_1523780043){ if($GLOBALS['____908293621'][16]($_1523780043) <= 0) return true; self::__557755087(); return($GLOBALS['____908293621'][17]($_1523780043, self::$_1648334508[___1205907454(26)]) && self::$_1648334508[___1205907454(27)][$_1523780043]);} public static function IsFeatureEditable($_1523780043){ if($GLOBALS['____908293621'][18]($_1523780043) <= 0) return true; self::__557755087(); if(!$GLOBALS['____908293621'][19]($_1523780043, self::$_784330868)) return true; if(self::$_784330868[$_1523780043] == ___1205907454(28)) $_1006949086= array(___1205907454(29)); elseif($GLOBALS['____908293621'][20](self::$_784330868[$_1523780043], self::$_1648334508[___1205907454(30)])) $_1006949086= self::$_1648334508[___1205907454(31)][self::$_784330868[$_1523780043]]; else $_1006949086= array(___1205907454(32)); if($_1006949086[(988-2*494)] != ___1205907454(33) && $_1006949086[(1220/2-610)] != ___1205907454(34)){ return false;} elseif($_1006949086[(184*2-368)] == ___1205907454(35)){ if($_1006949086[round(0+0.5+0.5)]< $GLOBALS['____908293621'][21]((1172/2-586),(758-2*379),(1424/2-712), Date(___1205907454(36)), $GLOBALS['____908293621'][22](___1205907454(37))- self::$_148789789, $GLOBALS['____908293621'][23](___1205907454(38)))){ if(!isset($_1006949086[round(0+2)]) ||!$_1006949086[round(0+0.5+0.5+0.5+0.5)]) self::__60063368(self::$_784330868[$_1523780043]); return false;}} return true;} private static function __413272710($_1523780043, $_1409756478){ if($GLOBALS['____908293621'][24]("CBXFeatures", "On".$_1523780043."SettingsChange")) $GLOBALS['____908293621'][25](array("CBXFeatures", "On".$_1523780043."SettingsChange"), array($_1523780043, $_1409756478)); $_792783395= $GLOBALS['_____114420438'][0](___1205907454(39), ___1205907454(40).$_1523780043.___1205907454(41)); while($_1127090625= $_792783395->Fetch()) $GLOBALS['_____114420438'][1]($_1127090625, array($_1523780043, $_1409756478));} public static function SetFeatureEnabled($_1523780043, $_1409756478= true, $_1290972858= true){ if($GLOBALS['____908293621'][26]($_1523780043) <= 0) return; if(!self::IsFeatureEditable($_1523780043)) $_1409756478= false; $_1409756478=($_1409756478? true: false); self::__557755087(); $_1569211073=(!$GLOBALS['____908293621'][27]($_1523780043, self::$_1648334508[___1205907454(42)]) && $_1409756478 || $GLOBALS['____908293621'][28]($_1523780043, self::$_1648334508[___1205907454(43)]) && $_1409756478 != self::$_1648334508[___1205907454(44)][$_1523780043]); self::$_1648334508[___1205907454(45)][$_1523780043]= $_1409756478; $_1624041312= $GLOBALS['____908293621'][29](self::$_1648334508); $_1624041312= $GLOBALS['____908293621'][30]($_1624041312); COption::SetOptionString(___1205907454(46), ___1205907454(47), $_1624041312); if($_1569211073 && $_1290972858) self::__413272710($_1523780043, $_1409756478);} private static function __60063368($_826357854){ if($GLOBALS['____908293621'][31]($_826357854) <= 0 || $_826357854 == "Portal") return; self::__557755087(); if(!$GLOBALS['____908293621'][32]($_826357854, self::$_1648334508[___1205907454(48)]) || $GLOBALS['____908293621'][33]($_826357854, self::$_1648334508[___1205907454(49)]) && self::$_1648334508[___1205907454(50)][$_826357854][(892-2*446)] != ___1205907454(51)) return; if(isset(self::$_1648334508[___1205907454(52)][$_826357854][round(0+0.66666666666667+0.66666666666667+0.66666666666667)]) && self::$_1648334508[___1205907454(53)][$_826357854][round(0+0.4+0.4+0.4+0.4+0.4)]) return; $_134907010= array(); if($GLOBALS['____908293621'][34]($_826357854, self::$_1006728797) && $GLOBALS['____908293621'][35](self::$_1006728797[$_826357854])){ foreach(self::$_1006728797[$_826357854] as $_1523780043){ if($GLOBALS['____908293621'][36]($_1523780043, self::$_1648334508[___1205907454(54)]) && self::$_1648334508[___1205907454(55)][$_1523780043]){ self::$_1648334508[___1205907454(56)][$_1523780043]= false; $_134907010[]= array($_1523780043, false);}} self::$_1648334508[___1205907454(57)][$_826357854][round(0+0.4+0.4+0.4+0.4+0.4)]= true;} $_1624041312= $GLOBALS['____908293621'][37](self::$_1648334508); $_1624041312= $GLOBALS['____908293621'][38]($_1624041312); COption::SetOptionString(___1205907454(58), ___1205907454(59), $_1624041312); foreach($_134907010 as $_777065327) self::__413272710($_777065327[(986-2*493)], $_777065327[round(0+0.25+0.25+0.25+0.25)]);} public static function ModifyFeaturesSettings($_1961714582, $_553949130){ self::__557755087(); foreach($_1961714582 as $_826357854 => $_1291206895) self::$_1648334508[___1205907454(60)][$_826357854]= $_1291206895; $_134907010= array(); foreach($_553949130 as $_1523780043 => $_1409756478){ if(!$GLOBALS['____908293621'][39]($_1523780043, self::$_1648334508[___1205907454(61)]) && $_1409756478 || $GLOBALS['____908293621'][40]($_1523780043, self::$_1648334508[___1205907454(62)]) && $_1409756478 != self::$_1648334508[___1205907454(63)][$_1523780043]) $_134907010[]= array($_1523780043, $_1409756478); self::$_1648334508[___1205907454(64)][$_1523780043]= $_1409756478;} $_1624041312= $GLOBALS['____908293621'][41](self::$_1648334508); $_1624041312= $GLOBALS['____908293621'][42]($_1624041312); COption::SetOptionString(___1205907454(65), ___1205907454(66), $_1624041312); self::$_1648334508= false; foreach($_134907010 as $_777065327) self::__413272710($_777065327[(1092/2-546)], $_777065327[round(0+0.5+0.5)]);} public static function SaveFeaturesSettings($_133058831, $_431764153){ self::__557755087(); $_1332183014= array(___1205907454(67) => array(), ___1205907454(68) => array()); if(!$GLOBALS['____908293621'][43]($_133058831)) $_133058831= array(); if(!$GLOBALS['____908293621'][44]($_431764153)) $_431764153= array(); if(!$GLOBALS['____908293621'][45](___1205907454(69), $_133058831)) $_133058831[]= ___1205907454(70); foreach(self::$_1006728797 as $_826357854 => $_553949130){ if($GLOBALS['____908293621'][46]($_826357854, self::$_1648334508[___1205907454(71)])) $_2046239198= self::$_1648334508[___1205907454(72)][$_826357854]; else $_2046239198=($_826357854 == ___1205907454(73))? array(___1205907454(74)): array(___1205907454(75)); if($_2046239198[(968-2*484)] == ___1205907454(76) || $_2046239198[min(232,0,77.333333333333)] == ___1205907454(77)){ $_1332183014[___1205907454(78)][$_826357854]= $_2046239198;} else{ if($GLOBALS['____908293621'][47]($_826357854, $_133058831)) $_1332183014[___1205907454(79)][$_826357854]= array(___1205907454(80), $GLOBALS['____908293621'][48]((1488/2-744),(936-2*468),(762-2*381), $GLOBALS['____908293621'][49](___1205907454(81)), $GLOBALS['____908293621'][50](___1205907454(82)), $GLOBALS['____908293621'][51](___1205907454(83)))); else $_1332183014[___1205907454(84)][$_826357854]= array(___1205907454(85));}} $_134907010= array(); foreach(self::$_784330868 as $_1523780043 => $_826357854){ if($_1332183014[___1205907454(86)][$_826357854][(153*2-306)] != ___1205907454(87) && $_1332183014[___1205907454(88)][$_826357854][(212*2-424)] != ___1205907454(89)){ $_1332183014[___1205907454(90)][$_1523780043]= false;} else{ if($_1332183014[___1205907454(91)][$_826357854][(1160/2-580)] == ___1205907454(92) && $_1332183014[___1205907454(93)][$_826357854][round(0+0.2+0.2+0.2+0.2+0.2)]< $GLOBALS['____908293621'][52]((838-2*419),(145*2-290),(1136/2-568), Date(___1205907454(94)), $GLOBALS['____908293621'][53](___1205907454(95))- self::$_148789789, $GLOBALS['____908293621'][54](___1205907454(96)))) $_1332183014[___1205907454(97)][$_1523780043]= false; else $_1332183014[___1205907454(98)][$_1523780043]= $GLOBALS['____908293621'][55]($_1523780043, $_431764153); if(!$GLOBALS['____908293621'][56]($_1523780043, self::$_1648334508[___1205907454(99)]) && $_1332183014[___1205907454(100)][$_1523780043] || $GLOBALS['____908293621'][57]($_1523780043, self::$_1648334508[___1205907454(101)]) && $_1332183014[___1205907454(102)][$_1523780043] != self::$_1648334508[___1205907454(103)][$_1523780043]) $_134907010[]= array($_1523780043, $_1332183014[___1205907454(104)][$_1523780043]);}} $_1624041312= $GLOBALS['____908293621'][58]($_1332183014); $_1624041312= $GLOBALS['____908293621'][59]($_1624041312); COption::SetOptionString(___1205907454(105), ___1205907454(106), $_1624041312); self::$_1648334508= false; foreach($_134907010 as $_777065327) self::__413272710($_777065327[min(96,0,32)], $_777065327[round(0+0.2+0.2+0.2+0.2+0.2)]);} public static function GetFeaturesList(){ self::__557755087(); $_1060227694= array(); foreach(self::$_1006728797 as $_826357854 => $_553949130){ if($GLOBALS['____908293621'][60]($_826357854, self::$_1648334508[___1205907454(107)])) $_2046239198= self::$_1648334508[___1205907454(108)][$_826357854]; else $_2046239198=($_826357854 == ___1205907454(109))? array(___1205907454(110)): array(___1205907454(111)); $_1060227694[$_826357854]= array( ___1205907454(112) => $_2046239198[(128*2-256)], ___1205907454(113) => $_2046239198[round(0+0.2+0.2+0.2+0.2+0.2)], ___1205907454(114) => array(),); $_1060227694[$_826357854][___1205907454(115)]= false; if($_1060227694[$_826357854][___1205907454(116)] == ___1205907454(117)){ $_1060227694[$_826357854][___1205907454(118)]= $GLOBALS['____908293621'][61](($GLOBALS['____908293621'][62]()- $_1060227694[$_826357854][___1205907454(119)])/ round(0+86400)); if($_1060227694[$_826357854][___1205907454(120)]> self::$_148789789) $_1060227694[$_826357854][___1205907454(121)]= true;} foreach($_553949130 as $_1523780043) $_1060227694[$_826357854][___1205907454(122)][$_1523780043]=(!$GLOBALS['____908293621'][63]($_1523780043, self::$_1648334508[___1205907454(123)]) || self::$_1648334508[___1205907454(124)][$_1523780043]);} return $_1060227694;} private static function __245524217($_279989318, $_1389309756){ if(IsModuleInstalled($_279989318) == $_1389309756) return true; $_1084719909= $_SERVER[___1205907454(125)].___1205907454(126).$_279989318.___1205907454(127); if(!$GLOBALS['____908293621'][64]($_1084719909)) return false; include_once($_1084719909); $_488687765= $GLOBALS['____908293621'][65](___1205907454(128), ___1205907454(129), $_279989318); if(!$GLOBALS['____908293621'][66]($_488687765)) return false; $_951715204= new $_488687765; if($_1389309756){ if(!$_951715204->InstallDB()) return false; $_951715204->InstallEvents(); if(!$_951715204->InstallFiles()) return false;} else{ if(CModule::IncludeModule(___1205907454(130))) CSearch::DeleteIndex($_279989318); UnRegisterModule($_279989318);} return true;} protected static function OnRequestsSettingsChange($_1523780043, $_1409756478){ self::__245524217("form", $_1409756478);} protected static function OnLearningSettingsChange($_1523780043, $_1409756478){ self::__245524217("learning", $_1409756478);} protected static function OnJabberSettingsChange($_1523780043, $_1409756478){ self::__245524217("xmpp", $_1409756478);} protected static function OnVideoConferenceSettingsChange($_1523780043, $_1409756478){ self::__245524217("video", $_1409756478);} protected static function OnBizProcSettingsChange($_1523780043, $_1409756478){ self::__245524217("bizprocdesigner", $_1409756478);} protected static function OnListsSettingsChange($_1523780043, $_1409756478){ self::__245524217("lists", $_1409756478);} protected static function OnWikiSettingsChange($_1523780043, $_1409756478){ self::__245524217("wiki", $_1409756478);} protected static function OnSupportSettingsChange($_1523780043, $_1409756478){ self::__245524217("support", $_1409756478);} protected static function OnControllerSettingsChange($_1523780043, $_1409756478){ self::__245524217("controller", $_1409756478);} protected static function OnAnalyticsSettingsChange($_1523780043, $_1409756478){ self::__245524217("statistic", $_1409756478);} protected static function OnVoteSettingsChange($_1523780043, $_1409756478){ self::__245524217("vote", $_1409756478);} protected static function OnFriendsSettingsChange($_1523780043, $_1409756478){ if($_1409756478) $_1688756489= "Y"; else $_1688756489= ___1205907454(131); $_723027386= CSite::GetList(($_182914697= ___1205907454(132)),($_913542010= ___1205907454(133)), array(___1205907454(134) => ___1205907454(135))); while($_156935017= $_723027386->Fetch()){ if(COption::GetOptionString(___1205907454(136), ___1205907454(137), ___1205907454(138), $_156935017[___1205907454(139)]) != $_1688756489){ COption::SetOptionString(___1205907454(140), ___1205907454(141), $_1688756489, false, $_156935017[___1205907454(142)]); COption::SetOptionString(___1205907454(143), ___1205907454(144), $_1688756489);}}} protected static function OnMicroBlogSettingsChange($_1523780043, $_1409756478){ if($_1409756478) $_1688756489= "Y"; else $_1688756489= ___1205907454(145); $_723027386= CSite::GetList(($_182914697= ___1205907454(146)),($_913542010= ___1205907454(147)), array(___1205907454(148) => ___1205907454(149))); while($_156935017= $_723027386->Fetch()){ if(COption::GetOptionString(___1205907454(150), ___1205907454(151), ___1205907454(152), $_156935017[___1205907454(153)]) != $_1688756489){ COption::SetOptionString(___1205907454(154), ___1205907454(155), $_1688756489, false, $_156935017[___1205907454(156)]); COption::SetOptionString(___1205907454(157), ___1205907454(158), $_1688756489);} if(COption::GetOptionString(___1205907454(159), ___1205907454(160), ___1205907454(161), $_156935017[___1205907454(162)]) != $_1688756489){ COption::SetOptionString(___1205907454(163), ___1205907454(164), $_1688756489, false, $_156935017[___1205907454(165)]); COption::SetOptionString(___1205907454(166), ___1205907454(167), $_1688756489);}}} protected static function OnPersonalFilesSettingsChange($_1523780043, $_1409756478){ if($_1409756478) $_1688756489= "Y"; else $_1688756489= ___1205907454(168); $_723027386= CSite::GetList(($_182914697= ___1205907454(169)),($_913542010= ___1205907454(170)), array(___1205907454(171) => ___1205907454(172))); while($_156935017= $_723027386->Fetch()){ if(COption::GetOptionString(___1205907454(173), ___1205907454(174), ___1205907454(175), $_156935017[___1205907454(176)]) != $_1688756489){ COption::SetOptionString(___1205907454(177), ___1205907454(178), $_1688756489, false, $_156935017[___1205907454(179)]); COption::SetOptionString(___1205907454(180), ___1205907454(181), $_1688756489);}}} protected static function OnPersonalBlogSettingsChange($_1523780043, $_1409756478){ if($_1409756478) $_1688756489= "Y"; else $_1688756489= ___1205907454(182); $_723027386= CSite::GetList(($_182914697= ___1205907454(183)),($_913542010= ___1205907454(184)), array(___1205907454(185) => ___1205907454(186))); while($_156935017= $_723027386->Fetch()){ if(COption::GetOptionString(___1205907454(187), ___1205907454(188), ___1205907454(189), $_156935017[___1205907454(190)]) != $_1688756489){ COption::SetOptionString(___1205907454(191), ___1205907454(192), $_1688756489, false, $_156935017[___1205907454(193)]); COption::SetOptionString(___1205907454(194), ___1205907454(195), $_1688756489);}}} protected static function OnPersonalPhotoSettingsChange($_1523780043, $_1409756478){ if($_1409756478) $_1688756489= "Y"; else $_1688756489= ___1205907454(196); $_723027386= CSite::GetList(($_182914697= ___1205907454(197)),($_913542010= ___1205907454(198)), array(___1205907454(199) => ___1205907454(200))); while($_156935017= $_723027386->Fetch()){ if(COption::GetOptionString(___1205907454(201), ___1205907454(202), ___1205907454(203), $_156935017[___1205907454(204)]) != $_1688756489){ COption::SetOptionString(___1205907454(205), ___1205907454(206), $_1688756489, false, $_156935017[___1205907454(207)]); COption::SetOptionString(___1205907454(208), ___1205907454(209), $_1688756489);}}} protected static function OnPersonalForumSettingsChange($_1523780043, $_1409756478){ if($_1409756478) $_1688756489= "Y"; else $_1688756489= ___1205907454(210); $_723027386= CSite::GetList(($_182914697= ___1205907454(211)),($_913542010= ___1205907454(212)), array(___1205907454(213) => ___1205907454(214))); while($_156935017= $_723027386->Fetch()){ if(COption::GetOptionString(___1205907454(215), ___1205907454(216), ___1205907454(217), $_156935017[___1205907454(218)]) != $_1688756489){ COption::SetOptionString(___1205907454(219), ___1205907454(220), $_1688756489, false, $_156935017[___1205907454(221)]); COption::SetOptionString(___1205907454(222), ___1205907454(223), $_1688756489);}}} protected static function OnTasksSettingsChange($_1523780043, $_1409756478){ if($_1409756478) $_1688756489= "Y"; else $_1688756489= ___1205907454(224); $_723027386= CSite::GetList(($_182914697= ___1205907454(225)),($_913542010= ___1205907454(226)), array(___1205907454(227) => ___1205907454(228))); while($_156935017= $_723027386->Fetch()){ if(COption::GetOptionString(___1205907454(229), ___1205907454(230), ___1205907454(231), $_156935017[___1205907454(232)]) != $_1688756489){ COption::SetOptionString(___1205907454(233), ___1205907454(234), $_1688756489, false, $_156935017[___1205907454(235)]); COption::SetOptionString(___1205907454(236), ___1205907454(237), $_1688756489);} if(COption::GetOptionString(___1205907454(238), ___1205907454(239), ___1205907454(240), $_156935017[___1205907454(241)]) != $_1688756489){ COption::SetOptionString(___1205907454(242), ___1205907454(243), $_1688756489, false, $_156935017[___1205907454(244)]); COption::SetOptionString(___1205907454(245), ___1205907454(246), $_1688756489);}} self::__245524217(___1205907454(247), $_1409756478);} protected static function OnCalendarSettingsChange($_1523780043, $_1409756478){ if($_1409756478) $_1688756489= "Y"; else $_1688756489= ___1205907454(248); $_723027386= CSite::GetList(($_182914697= ___1205907454(249)),($_913542010= ___1205907454(250)), array(___1205907454(251) => ___1205907454(252))); while($_156935017= $_723027386->Fetch()){ if(COption::GetOptionString(___1205907454(253), ___1205907454(254), ___1205907454(255), $_156935017[___1205907454(256)]) != $_1688756489){ COption::SetOptionString(___1205907454(257), ___1205907454(258), $_1688756489, false, $_156935017[___1205907454(259)]); COption::SetOptionString(___1205907454(260), ___1205907454(261), $_1688756489);} if(COption::GetOptionString(___1205907454(262), ___1205907454(263), ___1205907454(264), $_156935017[___1205907454(265)]) != $_1688756489){ COption::SetOptionString(___1205907454(266), ___1205907454(267), $_1688756489, false, $_156935017[___1205907454(268)]); COption::SetOptionString(___1205907454(269), ___1205907454(270), $_1688756489);}}} protected static function OnSMTPSettingsChange($_1523780043, $_1409756478){ self::__245524217("mail", $_1409756478);} protected static function OnExtranetSettingsChange($_1523780043, $_1409756478){ $_931511091= COption::GetOptionString("extranet", "extranet_site", ""); if($_931511091){ $_1977777665= new CSite; $_1977777665->Update($_931511091, array(___1205907454(271) =>($_1409756478? ___1205907454(272): ___1205907454(273))));} self::__245524217(___1205907454(274), $_1409756478);} protected static function OnDAVSettingsChange($_1523780043, $_1409756478){ self::__245524217("dav", $_1409756478);} protected static function OntimemanSettingsChange($_1523780043, $_1409756478){ self::__245524217("timeman", $_1409756478);} protected static function Onintranet_sharepointSettingsChange($_1523780043, $_1409756478){ if($_1409756478){ RegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", "intranet", "CIntranetEventHandlers", "SPRegisterUpdatedItem"); RegisterModuleDependences(___1205907454(275), ___1205907454(276), ___1205907454(277), ___1205907454(278), ___1205907454(279)); CAgent::AddAgent(___1205907454(280), ___1205907454(281), ___1205907454(282), round(0+125+125+125+125)); CAgent::AddAgent(___1205907454(283), ___1205907454(284), ___1205907454(285), round(0+150+150)); CAgent::AddAgent(___1205907454(286), ___1205907454(287), ___1205907454(288), round(0+3600));} else{ UnRegisterModuleDependences(___1205907454(289), ___1205907454(290), ___1205907454(291), ___1205907454(292), ___1205907454(293)); UnRegisterModuleDependences(___1205907454(294), ___1205907454(295), ___1205907454(296), ___1205907454(297), ___1205907454(298)); CAgent::RemoveAgent(___1205907454(299), ___1205907454(300)); CAgent::RemoveAgent(___1205907454(301), ___1205907454(302)); CAgent::RemoveAgent(___1205907454(303), ___1205907454(304));}} protected static function OncrmSettingsChange($_1523780043, $_1409756478){ if($_1409756478) COption::SetOptionString("crm", "form_features", "Y"); self::__245524217(___1205907454(305), $_1409756478);} protected static function OnClusterSettingsChange($_1523780043, $_1409756478){ self::__245524217("cluster", $_1409756478);} protected static function OnMultiSitesSettingsChange($_1523780043, $_1409756478){ if($_1409756478) RegisterModuleDependences("main", "OnBeforeProlog", "main", "CWizardSolPanelIntranet", "ShowPanel", 100, "/modules/intranet/panel_button.php"); else UnRegisterModuleDependences(___1205907454(306), ___1205907454(307), ___1205907454(308), ___1205907454(309), ___1205907454(310), ___1205907454(311));} protected static function OnIdeaSettingsChange($_1523780043, $_1409756478){ self::__245524217("idea", $_1409756478);} protected static function OnMeetingSettingsChange($_1523780043, $_1409756478){ self::__245524217("meeting", $_1409756478);} protected static function OnXDImportSettingsChange($_1523780043, $_1409756478){ self::__245524217("xdimport", $_1409756478);}} $GLOBALS['____908293621'][67](___1205907454(312), ___1205907454(313));/**/			//Do not remove this

//component 2.0 template engines
$GLOBALS["arCustomTemplateEngines"] = [];

require_once(__DIR__."/autoload.php");
require_once(__DIR__."/classes/general/menu.php");
require_once(__DIR__."/classes/mysql/usertype.php");

if(file_exists(($_fname = __DIR__."/classes/general/update_db_updater.php")))
{
	$US_HOST_PROCESS_MAIN = False;
	include($_fname);
}

if(file_exists(($_fname = $_SERVER["DOCUMENT_ROOT"]."/bitrix/init.php")))
	include_once($_fname);

if(($_fname = getLocalPath("php_interface/init.php", BX_PERSONAL_ROOT)) !== false)
	include_once($_SERVER["DOCUMENT_ROOT"].$_fname);

if(($_fname = getLocalPath("php_interface/".SITE_ID."/init.php", BX_PERSONAL_ROOT)) !== false)
	include_once($_SERVER["DOCUMENT_ROOT"].$_fname);

if(!defined("BX_FILE_PERMISSIONS"))
	define("BX_FILE_PERMISSIONS", 0644);
if(!defined("BX_DIR_PERMISSIONS"))
	define("BX_DIR_PERMISSIONS", 0755);

//global var, is used somewhere
$GLOBALS["sDocPath"] = $GLOBALS["APPLICATION"]->GetCurPage();

if((!(defined("STATISTIC_ONLY") && STATISTIC_ONLY && mb_substr($GLOBALS["APPLICATION"]->GetCurPage(), 0, mb_strlen(BX_ROOT."/admin/")) != BX_ROOT."/admin/")) && COption::GetOptionString("main", "include_charset", "Y")=="Y" && LANG_CHARSET <> '')
	header("Content-Type: text/html; charset=".LANG_CHARSET);

if(COption::GetOptionString("main", "set_p3p_header", "Y")=="Y")
	header("P3P: policyref=\"/bitrix/p3p.xml\", CP=\"NON DSP COR CUR ADM DEV PSA PSD OUR UNR BUS UNI COM NAV INT DEM STA\"");

header("X-Powered-CMS: Bitrix Site Manager (".(LICENSE_KEY == "DEMO"? "DEMO" : md5("BITRIX".LICENSE_KEY."LICENCE")).")");
if (COption::GetOptionString("main", "update_devsrv", "") == "Y")
	header("X-DevSrv-CMS: Bitrix");

define("BX_CRONTAB_SUPPORT", defined("BX_CRONTAB"));

//agents
if(COption::GetOptionString("main", "check_agents", "Y") == "Y")
{
	$application->addBackgroundJob(["CAgent", "CheckAgents"], [], \Bitrix\Main\Application::JOB_PRIORITY_LOW);
}

//send email events
if(COption::GetOptionString("main", "check_events", "Y") !== "N")
{
	$application->addBackgroundJob(['\Bitrix\Main\Mail\EventManager', 'checkEvents'], [], \Bitrix\Main\Application::JOB_PRIORITY_LOW-1);
}

$healerOfEarlySessionStart = new HealerEarlySessionStart();
$healerOfEarlySessionStart->process($application->getKernelSession());

$kernelSession = $application->getKernelSession();
$kernelSession->start();
$application->getSessionLocalStorageManager()->setUniqueId($kernelSession->getId());

foreach (GetModuleEvents("main", "OnPageStart", true) as $arEvent)
	ExecuteModuleEventEx($arEvent);

//define global user object
$GLOBALS["USER"] = new CUser;

//session control from group policy
$arPolicy = $GLOBALS["USER"]->GetSecurityPolicy();
$currTime = time();
if(
	(
		//IP address changed
		$kernelSession['SESS_IP']
		&& $arPolicy["SESSION_IP_MASK"] <> ''
		&& (
			(ip2long($arPolicy["SESSION_IP_MASK"]) & ip2long($kernelSession['SESS_IP']))
			!=
			(ip2long($arPolicy["SESSION_IP_MASK"]) & ip2long($_SERVER['REMOTE_ADDR']))
		)
	)
	||
	(
		//session timeout
		$arPolicy["SESSION_TIMEOUT"]>0
		&& $kernelSession['SESS_TIME']>0
		&& $currTime-$arPolicy["SESSION_TIMEOUT"]*60 > $kernelSession['SESS_TIME']
	)
	||
	(
		//signed session
		isset($kernelSession["BX_SESSION_SIGN"])
		&& $kernelSession["BX_SESSION_SIGN"] <> bitrix_sess_sign()
	)
	||
	(
		//session manually expired, e.g. in $User->LoginHitByHash
		isSessionExpired()
	)
)
{
	$compositeSessionManager = $application->getCompositeSessionManager();
	$compositeSessionManager->destroy();

	$application->getSession()->setId(md5(uniqid(rand(), true)));
	$compositeSessionManager->start();

	$GLOBALS["USER"] = new CUser;
}
$kernelSession['SESS_IP'] = $_SERVER['REMOTE_ADDR'];
if (empty($kernelSession['SESS_TIME']))
{
	$kernelSession['SESS_TIME'] = $currTime;
}
elseif (($currTime - $kernelSession['SESS_TIME']) > 60)
{
	$kernelSession['SESS_TIME'] = $currTime;
}
if(!isset($kernelSession["BX_SESSION_SIGN"]))
	$kernelSession["BX_SESSION_SIGN"] = bitrix_sess_sign();

//session control from security module
if(
	(COption::GetOptionString("main", "use_session_id_ttl", "N") == "Y")
	&& (COption::GetOptionInt("main", "session_id_ttl", 0) > 0)
	&& !defined("BX_SESSION_ID_CHANGE")
)
{
	if(!isset($kernelSession['SESS_ID_TIME']))
	{
		$kernelSession['SESS_ID_TIME'] = $currTime;
	}
	elseif(($kernelSession['SESS_ID_TIME'] + COption::GetOptionInt("main", "session_id_ttl")) < $kernelSession['SESS_TIME'])
	{
		$compositeSessionManager = $application->getCompositeSessionManager();
		$compositeSessionManager->regenerateId();

		$kernelSession['SESS_ID_TIME'] = $currTime;
	}
}

define("BX_STARTED", true);

if (isset($kernelSession['BX_ADMIN_LOAD_AUTH']))
{
	define('ADMIN_SECTION_LOAD_AUTH', 1);
	unset($kernelSession['BX_ADMIN_LOAD_AUTH']);
}

if(!defined("NOT_CHECK_PERMISSIONS") || NOT_CHECK_PERMISSIONS!==true)
{
	$doLogout = isset($_REQUEST["logout"]) && (strtolower($_REQUEST["logout"]) == "yes");

	if($doLogout && $GLOBALS["USER"]->IsAuthorized())
	{
		$secureLogout = (\Bitrix\Main\Config\Option::get("main", "secure_logout", "N") == "Y");

		if(!$secureLogout || check_bitrix_sessid())
		{
			$GLOBALS["USER"]->Logout();
			LocalRedirect($GLOBALS["APPLICATION"]->GetCurPageParam('', array('logout', 'sessid')));
		}
	}

	// authorize by cookies
	if(!$GLOBALS["USER"]->IsAuthorized())
	{
		$GLOBALS["USER"]->LoginByCookies();
	}

	$arAuthResult = false;

	//http basic and digest authorization
	if(($httpAuth = $GLOBALS["USER"]->LoginByHttpAuth()) !== null)
	{
		$arAuthResult = $httpAuth;
		$GLOBALS["APPLICATION"]->SetAuthResult($arAuthResult);
	}

	//Authorize user from authorization html form
	//Only POST is accepted
	if(isset($_POST["AUTH_FORM"]) && $_POST["AUTH_FORM"] <> '')
	{
		$bRsaError = false;
		if(COption::GetOptionString('main', 'use_encrypted_auth', 'N') == 'Y')
		{
			//possible encrypted user password
			$sec = new CRsaSecurity();
			if(($arKeys = $sec->LoadKeys()))
			{
				$sec->SetKeys($arKeys);
				$errno = $sec->AcceptFromForm(['USER_PASSWORD', 'USER_CONFIRM_PASSWORD', 'USER_CURRENT_PASSWORD']);
				if($errno == CRsaSecurity::ERROR_SESS_CHECK)
					$arAuthResult = array("MESSAGE"=>GetMessage("main_include_decode_pass_sess"), "TYPE"=>"ERROR");
				elseif($errno < 0)
					$arAuthResult = array("MESSAGE"=>GetMessage("main_include_decode_pass_err", array("#ERRCODE#"=>$errno)), "TYPE"=>"ERROR");

				if($errno < 0)
					$bRsaError = true;
			}
		}

		if($bRsaError == false)
		{
			if(!defined("ADMIN_SECTION") || ADMIN_SECTION !== true)
				$USER_LID = SITE_ID;
			else
				$USER_LID = false;

			if($_POST["TYPE"] == "AUTH")
			{
				$arAuthResult = $GLOBALS["USER"]->Login($_POST["USER_LOGIN"], $_POST["USER_PASSWORD"], $_POST["USER_REMEMBER"]);
			}
			elseif($_POST["TYPE"] == "OTP")
			{
				$arAuthResult = $GLOBALS["USER"]->LoginByOtp($_POST["USER_OTP"], $_POST["OTP_REMEMBER"], $_POST["captcha_word"], $_POST["captcha_sid"]);
			}
			elseif($_POST["TYPE"] == "SEND_PWD")
			{
				$arAuthResult = CUser::SendPassword($_POST["USER_LOGIN"], $_POST["USER_EMAIL"], $USER_LID, $_POST["captcha_word"], $_POST["captcha_sid"], $_POST["USER_PHONE_NUMBER"]);
			}
			elseif($_POST["TYPE"] == "CHANGE_PWD")
			{
				$arAuthResult = $GLOBALS["USER"]->ChangePassword($_POST["USER_LOGIN"], $_POST["USER_CHECKWORD"], $_POST["USER_PASSWORD"], $_POST["USER_CONFIRM_PASSWORD"], $USER_LID, $_POST["captcha_word"], $_POST["captcha_sid"], true, $_POST["USER_PHONE_NUMBER"], $_POST["USER_CURRENT_PASSWORD"]);
			}
			elseif(COption::GetOptionString("main", "new_user_registration", "N") == "Y" && $_POST["TYPE"] == "REGISTRATION" && (!defined("ADMIN_SECTION") || ADMIN_SECTION !== true))
			{
				$arAuthResult = $GLOBALS["USER"]->Register($_POST["USER_LOGIN"], $_POST["USER_NAME"], $_POST["USER_LAST_NAME"], $_POST["USER_PASSWORD"], $_POST["USER_CONFIRM_PASSWORD"], $_POST["USER_EMAIL"], $USER_LID, $_POST["captcha_word"], $_POST["captcha_sid"], false, $_POST["USER_PHONE_NUMBER"]);
			}

			if($_POST["TYPE"] == "AUTH" || $_POST["TYPE"] == "OTP")
			{
				//special login form in the control panel
				if($arAuthResult === true && defined('ADMIN_SECTION') && ADMIN_SECTION === true)
				{
					//store cookies for next hit (see CMain::GetSpreadCookieHTML())
					$GLOBALS["APPLICATION"]->StoreCookies();
					$kernelSession['BX_ADMIN_LOAD_AUTH'] = true;

					// die() follows
					CMain::FinalActions('<script type="text/javascript">window.onload=function(){(window.BX || window.parent.BX).AUTHAGENT.setAuthResult(false);};</script>');
				}
			}
		}
		$GLOBALS["APPLICATION"]->SetAuthResult($arAuthResult);
	}
	elseif(!$GLOBALS["USER"]->IsAuthorized())
	{
		//Authorize by unique URL
		$GLOBALS["USER"]->LoginHitByHash();
	}
}

//logout or re-authorize the user if something importand has changed
$GLOBALS["USER"]->CheckAuthActions();

//magic short URI
if(defined("BX_CHECK_SHORT_URI") && BX_CHECK_SHORT_URI && CBXShortUri::CheckUri())
{
	//local redirect inside
	die();
}

//application password scope control
if(($applicationID = $GLOBALS["USER"]->GetParam("APPLICATION_ID")) !== null)
{
	$appManager = \Bitrix\Main\Authentication\ApplicationManager::getInstance();
	if($appManager->checkScope($applicationID) !== true)
	{
		$event = new \Bitrix\Main\Event("main", "onApplicationScopeError", Array('APPLICATION_ID' => $applicationID));
		$event->send();

		CHTTP::SetStatus("403 Forbidden");
		die();
	}
}

//define the site template
if(!defined("ADMIN_SECTION") || ADMIN_SECTION !== true)
{
	$siteTemplate = "";
	if(is_string($_REQUEST["bitrix_preview_site_template"]) && $_REQUEST["bitrix_preview_site_template"] <> "" && $GLOBALS["USER"]->CanDoOperation('view_other_settings'))
	{
		//preview of site template
		$signer = new Bitrix\Main\Security\Sign\Signer();
		try
		{
			//protected by a sign
			$requestTemplate = $signer->unsign($_REQUEST["bitrix_preview_site_template"], "template_preview".bitrix_sessid());

			$aTemplates = CSiteTemplate::GetByID($requestTemplate);
			if($template = $aTemplates->Fetch())
			{
				$siteTemplate = $template["ID"];

				//preview of unsaved template
				if(isset($_GET['bx_template_preview_mode']) && $_GET['bx_template_preview_mode'] == 'Y' && $GLOBALS["USER"]->CanDoOperation('edit_other_settings'))
				{
					define("SITE_TEMPLATE_PREVIEW_MODE", true);
				}
			}
		}
		catch(\Bitrix\Main\Security\Sign\BadSignatureException $e)
		{
		}
	}
	if($siteTemplate == "")
	{
		$siteTemplate = CSite::GetCurTemplate();
	}
	define("SITE_TEMPLATE_ID", $siteTemplate);
	define("SITE_TEMPLATE_PATH", getLocalPath('templates/'.SITE_TEMPLATE_ID, BX_PERSONAL_ROOT));
}
else
{
	// prevents undefined constants
	define('SITE_TEMPLATE_ID', '.default');
	define('SITE_TEMPLATE_PATH', '/bitrix/templates/.default');
}

//magic parameters: show page creation time
if(isset($_GET["show_page_exec_time"]))
{
	if($_GET["show_page_exec_time"]=="Y" || $_GET["show_page_exec_time"]=="N")
		$kernelSession["SESS_SHOW_TIME_EXEC"] = $_GET["show_page_exec_time"];
}

//magic parameters: show included file processing time
if(isset($_GET["show_include_exec_time"]))
{
	if($_GET["show_include_exec_time"]=="Y" || $_GET["show_include_exec_time"]=="N")
		$kernelSession["SESS_SHOW_INCLUDE_TIME_EXEC"] = $_GET["show_include_exec_time"];
}

//magic parameters: show include areas
if(isset($_GET["bitrix_include_areas"]) && $_GET["bitrix_include_areas"] <> "")
	$GLOBALS["APPLICATION"]->SetShowIncludeAreas($_GET["bitrix_include_areas"]=="Y");

//magic sound
if($GLOBALS["USER"]->IsAuthorized())
{
	$cookie_prefix = COption::GetOptionString('main', 'cookie_name', 'BITRIX_SM');
	if(!isset($_COOKIE[$cookie_prefix.'_SOUND_LOGIN_PLAYED']))
		$GLOBALS["APPLICATION"]->set_cookie('SOUND_LOGIN_PLAYED', 'Y', 0);
}

//magic cache
\Bitrix\Main\Composite\Engine::shouldBeEnabled();

foreach(GetModuleEvents("main", "OnBeforeProlog", true) as $arEvent)
	ExecuteModuleEventEx($arEvent);

if((!defined("NOT_CHECK_PERMISSIONS") || NOT_CHECK_PERMISSIONS!==true) && (!defined("NOT_CHECK_FILE_PERMISSIONS") || NOT_CHECK_FILE_PERMISSIONS!==true))
{
	$real_path = $request->getScriptFile();

	if(!$GLOBALS["USER"]->CanDoFileOperation('fm_view_file', array(SITE_ID, $real_path)) || (defined("NEED_AUTH") && NEED_AUTH && !$GLOBALS["USER"]->IsAuthorized()))
	{
		/** @noinspection PhpUndefinedVariableInspection */
		if($GLOBALS["USER"]->IsAuthorized() && $arAuthResult["MESSAGE"] == '')
		{
			$arAuthResult = array("MESSAGE"=>GetMessage("ACCESS_DENIED").' '.GetMessage("ACCESS_DENIED_FILE", array("#FILE#"=>$real_path)), "TYPE"=>"ERROR");

			if(COption::GetOptionString("main", "event_log_permissions_fail", "N") === "Y")
			{
				CEventLog::Log("SECURITY", "USER_PERMISSIONS_FAIL", "main", $GLOBALS["USER"]->GetID(), $real_path);
			}
		}

		if(defined("ADMIN_SECTION") && ADMIN_SECTION==true)
		{
			if ($_REQUEST["mode"]=="list" || $_REQUEST["mode"]=="settings")
			{
				echo "<script>top.location='".$GLOBALS["APPLICATION"]->GetCurPage()."?".DeleteParam(array("mode"))."';</script>";
				die();
			}
			elseif ($_REQUEST["mode"]=="frame")
			{
				echo "<script type=\"text/javascript\">
					var w = (opener? opener.window:parent.window);
					w.location.href='".$GLOBALS["APPLICATION"]->GetCurPage()."?".DeleteParam(array("mode"))."';
				</script>";
				die();
			}
			elseif(defined("MOBILE_APP_ADMIN") && MOBILE_APP_ADMIN==true)
			{
				echo json_encode(Array("status"=>"failed"));
				die();
			}
		}

		/** @noinspection PhpUndefinedVariableInspection */
		$GLOBALS["APPLICATION"]->AuthForm($arAuthResult);
	}
}

/*ZDUyZmZNmZmYWVkMDZiMjlhZTZmMWE4NDZjODhmY2ViY2ZjNGU=*/$GLOBALS['____1531217825']= array(base64_decode('bXRfcmFuZA=='),base64_decode(''.'ZX'.'h'.'wb'.'G9'.'kZ'.'Q=='),base64_decode(''.'c'.'G'.'Fjaw=='),base64_decode(''.'bWQ'.'1'),base64_decode('Y29uc3Rh'.'b'.'nQ='),base64_decode('aGFzaF'.'9obW'.'F'.'j'),base64_decode('c'.'3'.'Ry'.'Y21w'),base64_decode(''.'aXNfb2JqZW'.'N0'),base64_decode('Y2Fsb'.'F91c2VyX2Z1b'.'mM='),base64_decode('Y2Fsb'.'F'.'91c'.'2'.'Vy'.'X2Z'.'1bmM='),base64_decode(''.'Y'.'2'.'Fs'.'bF91c2'.'VyX2Z1b'.'mM='),base64_decode(''.'Y2FsbF'.'91c2VyX2Z1'.'bmM'.'='),base64_decode('Y2Fs'.'bF91c2Vy'.'X2Z1bmM='));if(!function_exists(__NAMESPACE__.'\\___598383354')){function ___598383354($_1014337981){static $_2004118988= false; if($_2004118988 == false) $_2004118988=array('RE'.'I=','U0V'.'MRUNUIFZBTF'.'V'.'F'.'IEZST00gYl9vcHRpb24gV'.'0hFUk'.'U'.'g'.'T'.'kFN'.'RT0nf'.'l'.'B'.'BU'.'kFNX01BWF9VU0VSUy'.'c'.'gQU5EI'.'E1PRFVMRV9'.'JRD0nb'.'WFpb'.'i'.'cgQ'.'U5'.'EIFNJ'.'VEVfSU'.'Qg'.'SVM'.'g'.'TlVMTA==','Vk'.'F'.'MVUU=',''.'Lg'.'==','SCo=',''.'Ym'.'l'.'0cml'.'4','T'.'E'.'lDRU'.'5TRV9LR'.'Vk=','c2hhM'.'jU2',''.'V'.'VNFUg==',''.'VV'.'NFU'.'g='.'=',''.'VVNFUg'.'==','SXNB'.'dXRob3'.'JpemVk','VV'.'N'.'FUg'.'==','SX'.'NBZG'.'1pbg'.'==','QVB'.'Q'.'TElDQ'.'V'.'RJ'.'T'.'04'.'=','Um'.'V'.'zdGFydEJ'.'1Z'.'mZ'.'lcg==','TG9'.'jYWx'.'SZWRpcmVjdA==','L'.'2xpY2Vuc2VfcmVzdHJpY3Rpb24ucGh'.'w','XEJpdHJpeFx'.'NYWlu'.'XENvbmZpZ1x'.'PcHRpb246OnN'.'ld'.'A'.'==','bWFp'.'bg'.'==',''.'UE'.'FSQU1fTU'.'F'.'YX'.'1VTRVJ'.'T');return base64_decode($_2004118988[$_1014337981]);}};if($GLOBALS['____1531217825'][0](round(0+0.2+0.2+0.2+0.2+0.2), round(0+4+4+4+4+4)) == round(0+1.4+1.4+1.4+1.4+1.4)){ $_1841531126= $GLOBALS[___598383354(0)]->Query(___598383354(1), true); if($_883109250= $_1841531126->Fetch()){ $_1158191283= $_883109250[___598383354(2)]; list($_1547089109, $_136603177)= $GLOBALS['____1531217825'][1](___598383354(3), $_1158191283); $_419439656= $GLOBALS['____1531217825'][2](___598383354(4), $_1547089109); $_1111006476= ___598383354(5).$GLOBALS['____1531217825'][3]($GLOBALS['____1531217825'][4](___598383354(6))); $_1604198541= $GLOBALS['____1531217825'][5](___598383354(7), $_136603177, $_1111006476, true); if($GLOBALS['____1531217825'][6]($_1604198541, $_419439656) !== min(46,0,15.333333333333)){ if(isset($GLOBALS[___598383354(8)]) && $GLOBALS['____1531217825'][7]($GLOBALS[___598383354(9)]) && $GLOBALS['____1531217825'][8](array($GLOBALS[___598383354(10)], ___598383354(11))) &&!$GLOBALS['____1531217825'][9](array($GLOBALS[___598383354(12)], ___598383354(13)))){ $GLOBALS['____1531217825'][10](array($GLOBALS[___598383354(14)], ___598383354(15))); $GLOBALS['____1531217825'][11](___598383354(16), ___598383354(17), true);}}} else{ $GLOBALS['____1531217825'][12](___598383354(18), ___598383354(19), ___598383354(20), round(0+2.4+2.4+2.4+2.4+2.4));}}/**/       //Do not remove this

