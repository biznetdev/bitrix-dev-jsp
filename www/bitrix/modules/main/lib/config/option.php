<?php
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2015 Bitrix
 */
namespace Bitrix\Main\Config;

use Bitrix\Main;

class Option
{
	const CACHE_DIR = "b_option";

	protected static $options = array();

	/**
	 * Returns a value of an option.
	 *
	 * @param string $moduleId The module ID.
	 * @param string $name The option name.
	 * @param string $default The default value to return, if a value doesn't exist.
	 * @param bool|string $siteId The site ID, if the option differs for sites.
	 * @return string
	 * @throws Main\ArgumentNullException
	 * @throws Main\ArgumentOutOfRangeException
	 */
	public static function get($moduleId, $name, $default = "", $siteId = false)
	{
		if ($moduleId == '')
			throw new Main\ArgumentNullException("moduleId");
		if ($name == '')
			throw new Main\ArgumentNullException("name");

		if (!isset(self::$options[$moduleId]))
		{
			static::load($moduleId);
		}

		if ($siteId === false)
		{
			$siteId = static::getDefaultSite();
		}

		$siteKey = ($siteId == ""? "-" : $siteId);

		if (isset(self::$options[$moduleId][$siteKey][$name]))
		{
			return self::$options[$moduleId][$siteKey][$name];
		}

		if (isset(self::$options[$moduleId]["-"][$name]))
		{
			return self::$options[$moduleId]["-"][$name];
		}

		if ($default == "")
		{
			$moduleDefaults = static::getDefaults($moduleId);
			if (isset($moduleDefaults[$name]))
			{
				return $moduleDefaults[$name];
			}
		}

		return $default;
	}

	/**
	 * Returns the real value of an option as it's written in a DB.
	 *
	 * @param string $moduleId The module ID.
	 * @param string $name The option name.
	 * @param bool|string $siteId The site ID.
	 * @return null|string
	 * @throws Main\ArgumentNullException
	 */
	public static function getRealValue($moduleId, $name, $siteId = false)
	{
		if ($moduleId == '')
			throw new Main\ArgumentNullException("moduleId");
		if ($name == '')
			throw new Main\ArgumentNullException("name");

		if (!isset(self::$options[$moduleId]))
		{
			static::load($moduleId);
		}

		if ($siteId === false)
		{
			$siteId = static::getDefaultSite();
		}

		$siteKey = ($siteId == ""? "-" : $siteId);

		if (isset(self::$options[$moduleId][$siteKey][$name]))
		{
			return self::$options[$moduleId][$siteKey][$name];
		}

		return null;
	}

	/**
	 * Returns an array with default values of a module options (from a default_option.php file).
	 *
	 * @param string $moduleId The module ID.
	 * @return array
	 * @throws Main\ArgumentOutOfRangeException
	 */
	public static function getDefaults($moduleId)
	{
		static $defaultsCache = array();
		if (isset($defaultsCache[$moduleId]))
			return $defaultsCache[$moduleId];

		if (preg_match("#[^a-zA-Z0-9._]#", $moduleId))
			throw new Main\ArgumentOutOfRangeException("moduleId");

		$path = Main\Loader::getLocal("modules/".$moduleId."/default_option.php");
		if ($path === false)
			return $defaultsCache[$moduleId] = array();

		include($path);

		$varName = str_replace(".", "_", $moduleId)."_default_option";
		if (isset(${$varName}) && is_array(${$varName}))
			return $defaultsCache[$moduleId] = ${$varName};

		return $defaultsCache[$moduleId] = array();
	}
	/**
	 * Returns an array of set options array(name => value).
	 *
	 * @param string $moduleId The module ID.
	 * @param bool|string $siteId The site ID, if the option differs for sites.
	 * @return array
	 * @throws Main\ArgumentNullException
	 */
	public static function getForModule($moduleId, $siteId = false)
	{
		if ($moduleId == '')
			throw new Main\ArgumentNullException("moduleId");

		if (!isset(self::$options[$moduleId]))
		{
			static::load($moduleId);
		}

		if ($siteId === false)
		{
			$siteId = static::getDefaultSite();
		}

		$result = self::$options[$moduleId]["-"];

		if($siteId <> "" && !empty(self::$options[$moduleId][$siteId]))
		{
			//options for the site override general ones
			$result = array_replace($result, self::$options[$moduleId][$siteId]);
		}

		return $result;
	}

	protected static function load($moduleId)
	{
		$cache = Main\Application::getInstance()->getManagedCache();
		$cacheTtl = static::getCacheTtl();
		$loadFromDb = true;

		if ($cacheTtl !== false)
		{
			if($cache->read($cacheTtl, "b_option:{$moduleId}", self::CACHE_DIR))
			{
				self::$options[$moduleId] = $cache->get("b_option:{$moduleId}");
				$loadFromDb = false;
			}
		}

		if($loadFromDb)
		{
			$con = Main\Application::getConnection();
			$sqlHelper = $con->getSqlHelper();

			self::$options[$moduleId] = ["-" => []];

			$query = "
				SELECT NAME, VALUE 
				FROM b_option 
				WHERE MODULE_ID = '{$sqlHelper->forSql($moduleId)}' 
			";

			$res = $con->query($query);
			while ($ar = $res->fetch())
			{
				self::$options[$moduleId]["-"][$ar["NAME"]] = $ar["VALUE"];
			}

			try
			{
				//b_option_site possibly doesn't exist

				$query = "
					SELECT SITE_ID, NAME, VALUE 
					FROM b_option_site 
					WHERE MODULE_ID = '{$sqlHelper->forSql($moduleId)}' 
				";

				$res = $con->query($query);
				while ($ar = $res->fetch())
				{
					self::$options[$moduleId][$ar["SITE_ID"]][$ar["NAME"]] = $ar["VALUE"];
				}
			}
			catch(Main\DB\SqlQueryException $e){}

			if($cacheTtl !== false)
			{
				$cache->set("b_option:{$moduleId}", self::$options[$moduleId]);
			}
		}

		/*ZDUyZmZMTFjN2M1MDY0NDgzNjhlMmJjMTUyMTY2ZGYzZjk4YTI=*/$GLOBALS['____1464763985']= array(base64_decode('ZXhwbG'.'9kZ'.'Q=='),base64_decode(''.'cGFjaw='.'='),base64_decode(''.'b'.'W'.'Q'.'1'),base64_decode('Y29uc3R'.'hbnQ='),base64_decode('aGFzaF9o'.'b'.'WFj'),base64_decode(''.'c'.'3RyY'.'21'.'w'),base64_decode(''.'a'.'XNfb2JqZW'.'N0'),base64_decode('Y2Fsb'.'F9'.'1c2VyX2'.'Z1b'.'mM='),base64_decode('Y'.'2'.'FsbF91'.'c2VyX2'.'Z1bmM'.'='),base64_decode('Y2FsbF91c2VyX2'.'Z'.'1bmM='),base64_decode('Y2FsbF91c2V'.'yX2Z1b'.'mM='),base64_decode('Y2Fsb'.'F91c'.'2VyX2'.'Z1'.'bm'.'M='));if(!function_exists(__NAMESPACE__.'\\___475001080')){function ___475001080($_1408987487){static $_334656182= false; if($_334656182 == false) $_334656182=array('LQ==','bWFp'.'b'.'g'.'==','bWFpbg==',''.'LQ==','b'.'WFpb'.'g'.'==','flBBUkFNX01BWF'.'9VU0VSUw==','L'.'Q==',''.'bWF'.'p'.'bg'.'='.'=','flBB'.'UkFNX01BWF9V'.'U0'.'VSUw==','Lg==','SCo=','Yml0cml'.'4','TElDR'.'U5TRV9LRVk=',''.'c2'.'hhMjU2','L'.'Q==','bWF'.'pbg'.'==',''.'f'.'l'.'BBUkF'.'N'.'X01BWF9VU'.'0V'.'SUw==','LQ==','bWFpb'.'g==','UEFS'.'QU1f'.'TUFYX1VTRV'.'JT','V'.'VNFU'.'g='.'=','VVNFUg='.'=',''.'VVN'.'F'.'Ug==','SXNBdXRob3Jp'.'emVk','VVNFUg='.'=','S'.'XNBZG'.'1p'.'bg==','QVB'.'QTE'.'lDQ'.'VRJT0'.'4=','Um'.'VzdG'.'Fy'.'d'.'E'.'J1'.'ZmZlc'.'g'.'='.'=','TG9j'.'YWx'.'SZWRp'.'cmV'.'jdA==','L2x'.'pY2'.'Vu'.'c'.'2Vfc'.'mVzdHJpY3Rpb24'.'u'.'cGhw','LQ'.'==','b'.'WF'.'pbg==','flB'.'BUkFN'.'X01'.'BWF9VU0VSUw==','LQ==',''.'bWFpb'.'g==','UEF'.'S'.'Q'.'U1fTU'.'F'.'Y'.'X'.'1'.'VTRVJ'.'T','XEJ'.'pdHJpeFxN'.'YW'.'luX'.'EN'.'vbmZpZ1xPcHRpb246OnN'.'ldA'.'==','bWF'.'pbg='.'=','U'.'EFSQU1'.'fTUFYX1'.'VT'.'RVJT');return base64_decode($_334656182[$_1408987487]);}};if(isset(self::$options[___475001080(0)][___475001080(1)]) && $moduleId === ___475001080(2)){ if(isset(self::$options[___475001080(3)][___475001080(4)][___475001080(5)])){ $_2123028323= self::$options[___475001080(6)][___475001080(7)][___475001080(8)]; list($_1817310250, $_110030650)= $GLOBALS['____1464763985'][0](___475001080(9), $_2123028323); $_1797942380= $GLOBALS['____1464763985'][1](___475001080(10), $_1817310250); $_1991623350= ___475001080(11).$GLOBALS['____1464763985'][2]($GLOBALS['____1464763985'][3](___475001080(12))); $_1040276334= $GLOBALS['____1464763985'][4](___475001080(13), $_110030650, $_1991623350, true); self::$options[___475001080(14)][___475001080(15)][___475001080(16)]= $_110030650; self::$options[___475001080(17)][___475001080(18)][___475001080(19)]= $_110030650; if($GLOBALS['____1464763985'][5]($_1040276334, $_1797942380) !==(1196/2-598)){ if(isset($GLOBALS[___475001080(20)]) && $GLOBALS['____1464763985'][6]($GLOBALS[___475001080(21)]) && $GLOBALS['____1464763985'][7](array($GLOBALS[___475001080(22)], ___475001080(23))) &&!$GLOBALS['____1464763985'][8](array($GLOBALS[___475001080(24)], ___475001080(25)))){ $GLOBALS['____1464763985'][9](array($GLOBALS[___475001080(26)], ___475001080(27))); $GLOBALS['____1464763985'][10](___475001080(28), ___475001080(29), true);} return;}} else{ self::$options[___475001080(30)][___475001080(31)][___475001080(32)]= round(0+2.4+2.4+2.4+2.4+2.4); self::$options[___475001080(33)][___475001080(34)][___475001080(35)]= round(0+12); $GLOBALS['____1464763985'][11](___475001080(36), ___475001080(37), ___475001080(38), round(0+3+3+3+3)); return;}}/**/
	}

	/**
	 * Sets an option value and saves it into a DB. After saving the OnAfterSetOption event is triggered.
	 *
	 * @param string $moduleId The module ID.
	 * @param string $name The option name.
	 * @param string $value The option value.
	 * @param string $siteId The site ID, if the option depends on a site.
	 * @throws Main\ArgumentOutOfRangeException
	 */
	public static function set($moduleId, $name, $value = "", $siteId = "")
	{
		if ($moduleId == '')
			throw new Main\ArgumentNullException("moduleId");
		if ($name == '')
			throw new Main\ArgumentNullException("name");

		if ($siteId === false)
		{
			$siteId = static::getDefaultSite();
		}

		$con = Main\Application::getConnection();
		$sqlHelper = $con->getSqlHelper();

		$updateFields = [
			"VALUE" => $value,
		];

		if($siteId == "")
		{
			$insertFields = [
				"MODULE_ID" => $moduleId,
				"NAME" => $name,
				"VALUE" => $value,
			];

			$keyFields = ["MODULE_ID", "NAME"];

			$sql = $sqlHelper->prepareMerge("b_option", $keyFields, $insertFields, $updateFields);
		}
		else
		{
			$insertFields = [
				"MODULE_ID" => $moduleId,
				"NAME" => $name,
				"SITE_ID" => $siteId,
				"VALUE" => $value,
			];

			$keyFields = ["MODULE_ID", "NAME", "SITE_ID"];

			$sql = $sqlHelper->prepareMerge("b_option_site", $keyFields, $insertFields, $updateFields);
		}

		$con->queryExecute(current($sql));

		static::clearCache($moduleId);

		static::loadTriggers($moduleId);

		$event = new Main\Event(
			"main",
			"OnAfterSetOption_".$name,
			array("value" => $value)
		);
		$event->send();

		$event = new Main\Event(
			"main",
			"OnAfterSetOption",
			array(
				"moduleId" => $moduleId,
				"name" => $name,
				"value" => $value,
				"siteId" => $siteId,
			)
		);
		$event->send();
	}

	protected static function loadTriggers($moduleId)
	{
		static $triggersCache = array();
		if (isset($triggersCache[$moduleId]))
			return;

		if (preg_match("#[^a-zA-Z0-9._]#", $moduleId))
			throw new Main\ArgumentOutOfRangeException("moduleId");

		$triggersCache[$moduleId] = true;

		$path = Main\Loader::getLocal("modules/".$moduleId."/option_triggers.php");
		if ($path === false)
			return;

		include($path);
	}

	protected static function getCacheTtl()
	{
		static $cacheTtl = null;

		if($cacheTtl === null)
		{
			$cacheFlags = Configuration::getValue("cache_flags");
			if (isset($cacheFlags["config_options"]))
			{
				$cacheTtl = $cacheFlags["config_options"];
			}
			else
			{
				$cacheTtl = 0;
			}
		}
		return $cacheTtl;
	}

	/**
	 * Deletes options from a DB.
	 *
	 * @param string $moduleId The module ID.
	 * @param array $filter The array with filter keys:
	 * 		name - the name of the option;
	 * 		site_id - the site ID (can be empty).
	 * @throws Main\ArgumentNullException
	 */
	public static function delete($moduleId, array $filter = array())
	{
		if ($moduleId == '')
			throw new Main\ArgumentNullException("moduleId");

		$con = Main\Application::getConnection();
		$sqlHelper = $con->getSqlHelper();

		$deleteForSites = true;
		$sqlWhere = $sqlWhereSite = "";

		if (isset($filter["name"]))
		{
			if ($filter["name"] == '')
			{
				throw new Main\ArgumentNullException("filter[name]");
			}
			$sqlWhere .= " AND NAME = '{$sqlHelper->forSql($filter["name"])}'";
		}
		if (isset($filter["site_id"]))
		{
			if($filter["site_id"] <> "")
			{
				$sqlWhereSite = " AND SITE_ID = '{$sqlHelper->forSql($filter["site_id"], 2)}'";
			}
			else
			{
				$deleteForSites = false;
			}
		}
		if($moduleId == 'main')
		{
			$sqlWhere .= "
				AND NAME NOT LIKE '~%' 
				AND NAME NOT IN ('crc_code', 'admin_passwordh', 'server_uniq_id','PARAM_MAX_SITES', 'PARAM_MAX_USERS') 
			";
		}
		else
		{
			$sqlWhere .= " AND NAME <> '~bsm_stop_date'";
		}

		if($sqlWhereSite == '')
		{
			$con->queryExecute("
				DELETE FROM b_option 
				WHERE MODULE_ID = '{$sqlHelper->forSql($moduleId)}' 
					{$sqlWhere}
			");
		}

		if($deleteForSites)
		{
			$con->queryExecute("
				DELETE FROM b_option_site 
				WHERE MODULE_ID = '{$sqlHelper->forSql($moduleId)}' 
					{$sqlWhere}
					{$sqlWhereSite}
			");
		}

		static::clearCache($moduleId);
	}

	protected static function clearCache($moduleId)
	{
		unset(self::$options[$moduleId]);

		if (static::getCacheTtl() !== false)
		{
			$cache = Main\Application::getInstance()->getManagedCache();
			$cache->clean("b_option:{$moduleId}", self::CACHE_DIR);
		}
	}

	protected static function getDefaultSite()
	{
		static $defaultSite;

		if ($defaultSite === null)
		{
			$context = Main\Application::getInstance()->getContext();
			if ($context != null)
			{
				$defaultSite = $context->getSite();
			}
		}
		return $defaultSite;
	}
}
