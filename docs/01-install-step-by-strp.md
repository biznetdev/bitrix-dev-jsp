# Step-by-step instructions for installing the portal Bitrix24 

 The instruction describes the startup process of the Bitrix24 Docker image on the local machine. 

 ## Step 1: Check for installed programs 

 Make sure that you are locally installed and docker. For Linux users additionally[This will require installation of Docker Compose](https://docs.docker.com/compose/install/). 

```shell
$ docker --version
Docker version 18.09.2, build 6247962

$ docker-compose --version
docker-compose version 1.23.2, build 1110ad01
```

If you do not have Docker, use the official guide.

- [Windows](https://docs.docker.com/docker-for-windows/install/)
- [Linux (Ubuntu, CentOS, Debian)](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
- [Mac](https://docs.docker.com/docker-for-mac/install/)

> For Windows 10 users, before installing, you should pay attention to the version of your operating system. 
 > 
 > So, if you have `Windows 10 Pro` You will fit the usual version of the Docker CE, if you have` Windows 10 Home`, then you will need an additional installation `Docker Toolbox`. 
 > 
 > Follow the instructions of the official manual. 

 ## Step 2: Project Initialization 

 Create a new folder in which our project will be located, and go to it. 

 In the console it will look like this:

```shell
mkdir myproject
cd myproject
```

## Step 3: Creating a configuration file 

 In the project folder, create `docker-compose.yml` with the following contents:


```yml
version: '3'
services:
  web:
    image: "akopkesheshyan/bitrix24:latest"
    ports:
      - "80:80"
      - "443:443"
    cap_add:
      - SYS_ADMIN 
    security_opt:
      - seccomp:unconfined
    privileged: true
    volumes:
      - ./:/home/bitrix/www/local
    depends_on:
      - mysql
  mysql:
    image: mariadb
    healthcheck:
      test: "/usr/bin/mysql --user=root --password=+Tr+()8]!szl[HQIsoT5 --execute \"SHOW DATABASES;\""
      interval: 2s
      timeout: 20s
      retries: 10
    ports:
      - "3306:3306"
    environment:
      MYSQL_ROOT_PASSWORD: +Tr+()8]!szl[HQIsoT5
      MYSQL_DATABASE: sitemanager
      MYSQL_USER: bitrix
      MYSQL_PASSWORD: +Tr+()8]!szl[HQIsoT5
    command: ['--character-set-server=utf8', '--collation-server=utf8_unicode_ci', '--skip-character-set-client-handshake', '--sql-mode=']   
```

## Step 4: Launch Bitrix24 Docker 

 In the console, execute the command:

```shell
docker-compose up -d
```

The screen will receive a message about the successful launch of containers.

```shell
$ docker-compose up -d
Starting myproject_mysql_1 ... done
Starting myproject_tools_1 ... done
Starting myproject_web_1   ... done
```

In case of errors, make sure that you are in the project directory (see Step 2) and you have Docker Compose.

## Step 4: Installing the Portal

Open the browser and go to HTTP: // Localhost, you will see the Bitrix standard installer. As a result of his work, you will get a fully working portal.

All files posted in the project directory will be available in the `/ Local folder`. If you do not yet use this directory to develop your own solutions, it is strongly recommended to familiarize yourself with the official course ["Bitrix Framework Developer"] (https://dev.1c-bitrix.ru/learning/course/index.php?course_id=43&lesson_id= 2705 ​​& lesson_path = 3913.4776.2483.2705)