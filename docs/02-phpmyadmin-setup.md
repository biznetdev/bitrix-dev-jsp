# How to connect phpmyadmin 

 If you use PHPMYAdmin to work with the database, you can connect it via Docker Compose. 

## Step 1: Creating a configuration file 

 In the project folder, create `docker-compose.yml` with the following contents:

```yml
version: '3'
services:
  web:
    image: "akopkesheshyan/bitrix24:latest"
    ports:
      - "80:80"
      - "443:443"
    cap_add:
      - SYS_ADMIN 
    security_opt:
      - seccomp:unconfined
    privileged: true
    volumes:
      - ./:/home/bitrix/www/local
    depends_on:
      - mysql
  mysql:
    image: mariadb
    healthcheck:
      test: "/usr/bin/mysql --user=root --password=+Tr+()8]!szl[HQIsoT5 --execute \"SHOW DATABASES;\""
      interval: 2s
      timeout: 20s
      retries: 10
    ports:
      - "3306:3306"
    environment:
      MYSQL_ROOT_PASSWORD: +Tr+()8]!szl[HQIsoT5
      MYSQL_DATABASE: sitemanager
      MYSQL_USER: bitrix
      MYSQL_PASSWORD: +Tr+()8]!szl[HQIsoT5
    command: ['--character-set-server=utf8', '--collation-server=utf8_unicode_ci', '--skip-character-set-client-handshake', '--sql-mode=']
  phpmyadmin:
    image: phpmyadmin/phpmyadmin
    links:
      - mysql:mysql
    ports:
      - 8181:80
    environment:
      PMA_HOST: mysql
      MYSQL_USERNAME: bitrix
      MYSQL_PASSWORD: +Tr+()8]!szl[HQIsoT5
```

## Step 2: Launch Bitrix24 Docker 

 In the console, execute the command:

```shell
docker-compose up -d
```

The screen will receive a message about the successful launch of containers.

```shell
$ docker-compose up -d
Starting myproject_mysql_1 ... done
Starting myproject_tools_1 ... done
Starting myproject_web_1   ... done
Starting myproject_phpmyadmin_1 ... done
```

## Step 3: Connect to phpMyadmin 

 Open the browser and go to http: // Localhost: 8181, you will see the standard authorization window `phpmyAdmin`. 

 Use the entry parameters from the configuration file:

- `Username`: bitrix
- `Password`: +Tr+()8]!szl[HQIsoT5

## Additional Information 

 - [Official PHPMYAdmin assembly] (https://hub.docker.com/r/phpmyAdmin/phpmyAdmin/)